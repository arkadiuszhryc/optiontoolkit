import numpy as np
import math

print('Welcome in OptionToolkit - a suit for pricing options.')
print('This code would help your in pricing options with Monte Carlo method.')

#Asking user for mandatory data
rate = float(input('Enter risk-free rate: '))
time = float(eval(input('Enter time to maturity (in years; if your option will be executed i. e. in. 30 days, type "30/365": ')))
basePrice = float(input('Enter current price of underlying instrument: '))
dividentRate = float(input('Enter divident rate of underlying instrument: '))
diversity = float(input('Enter the diversity (standard deviation) of underlying instrument: '))
deltaTime = 1/252 #252 is average number of session days in year
numOfTrajectories = 100000
numOfDays = math.ceil(time/deltaTime)

#Checking if option has barriers
barrierOption = input('Is this a barrier option? [y, N] ')
if barrierOption:
    barrierOption = barrierOption.lower()[0]
    if barrierOption == 'y':
        lowerBarrier = input('Enter lower barrier price: ')
        if lowerBarrier: lowerBarrier = float(lowerBarrier)
        upperBarrier = float(input('Enter upper barrier price: '))
        if upperBarrier: upperBarrier = float(upperBarrier)
    else:
        lowerBarrier = False
        upperBarrier = False
        if barrierOption != 'n':
            print('Assuming that this is vanilla option')
else:
    lowerBarrier = False
    upperBarrier = False
    print('Assuming that this is vanilla option')

#Generating normal distribution array and mirroring it
#By mirroring we ensure that distribution is closly to mean
normDistArr = np.random.normal(loc = 0.0, scale = 1.0, size = ((numOfTrajectories // 2), numOfDays))
minusNormDistArr = normDistArr - 2 * normDistArr
normDistArr = np.concatenate((normDistArr, minusNormDistArr), axis = 0)

#Calculating possible option prices
rArr = (rate - dividentRate - (math.pow(diversity, 2) / 2)) * deltaTime + diversity * math.sqrt(deltaTime) * normDistArr
stArr = np.sum(rArr, axis = 1)
stArr = basePrice * np.exp(stArr)

for i in str(len(stArr)):
    if lowerBarrier:
        if stArr[i] < lowerBarrier: stArr[i] = 0
    if upperBarrier:
        if stArr[i] > upperBarrier: stArr[i] = 0

#Calculating price of option and discounting it
price = math.exp(-rate * time) * np.mean(stArr)
print('Option price: {}'.\
      format(round(price, 2)))