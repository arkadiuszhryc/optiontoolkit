import math
from scipy.stats import norm

print('Welcome in OptionToolkit - a suit for pricing options.')
print('This code would help your in pricing options with Black-Scholes-Merton model.')

#Asking user for mandatory data
rate = float(input('Enter risk-free rate: '))
time = float(eval(input('Enter time to maturity (in years; if your option will be executed i. e. in. 30 days, type "30/365": ')))
basePrice = float(input('Enter current price of underlying instrument: '))
strikePrice = float(input('Enter option striking price: '))
dividentRate = float(input('Enter divident rate of underlying instrument: '))
diversity = float(input('Enter the diversity (standard deviation) of underlying instrument: '))

#Calculating put and call option price
d1 = (math.log(basePrice / strikePrice) + (rate - dividentRate + (math.pow(diversity, 2) / 2)) * time) / (diversity * math.sqrt(time))
d2 = (math.log(basePrice / strikePrice) + (rate - dividentRate - (math.pow(diversity, 2) / 2)) * time) / (diversity * math.sqrt(time))

callPrice = (basePrice * math.exp(-dividentRate * time) * norm.cdf(d1)) - (strikePrice * math.exp(-rate * time) * norm.cdf(d2))
putPrice = (strikePrice * math.exp(-rate * time) * norm.cdf(-d2)) - (basePrice * math.exp(-dividentRate * time) * norm.cdf(-d1))
print('Call option price: {}\nPut option price: {}'.\
      format(round(callPrice, 2), round(putPrice, 2)))