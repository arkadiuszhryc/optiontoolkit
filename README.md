OptionToolkit
====

Bundle of python scripts for option analysis.
----

Currently supported analysis methods:

- Black-Scholes-Merton model,

- Monte Carlo method.

Project under MIT license.